package com.example.relationshipdemo.Service;

import com.example.relationshipdemo.Entity.Teacher;
import com.example.relationshipdemo.Repository.TeacherRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TeacherService {
    @Autowired
    TeacherRepository teacherRepository;


    public void save(Teacher teacher) {
         teacherRepository.save(teacher);
    }

    public List<Teacher> getAllTeacher() {
        return teacherRepository.findAll();
    }

    public Teacher findTeacherById(long teacherId) {
        return  teacherRepository.getOne(teacherId);
    }

}
