package com.example.relationshipdemo.Controller;

import com.example.relationshipdemo.Dto.DepartmentDto;
import com.example.relationshipdemo.Dto.TeacherDto;
import com.example.relationshipdemo.Entity.Teacher;
import com.example.relationshipdemo.Service.TeacherService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/teacher")
public class TeacherController {
    @Autowired
    TeacherService teacherService;

    @GetMapping("/addTeacher")
    public String addTeacher(Model model) {
        TeacherDto teacherDto = new TeacherDto();
        model.addAttribute("teacherDtoForm", teacherDto);

        return "teacher/add";
    }

    @PostMapping("/save")
    public String saveTeacher(@ModelAttribute TeacherDto teacherDtoForm) {
        Teacher teacher = new Teacher();
        BeanUtils.copyProperties(teacherDtoForm, teacher);

        teacherService.save(teacher);
        return "redirect:/teacher/addTeacher";
    }

    @GetMapping("/getAll")
    public String getAllTeacher(Model model) {
        List<Teacher> teacherList = teacherService.getAllTeacher();

        model.addAttribute("teacherDtoList", generateTeacherDtoList(teacherList));

        return "teacher/show";
    }


    // Helper Method
    private List<TeacherDto> generateTeacherDtoList(List<Teacher> teacherList) {
        List<TeacherDto> teacherDtoList = new ArrayList<>();
        for (Teacher teacher : teacherList) {
            TeacherDto teacherDto = new TeacherDto();
            BeanUtils.copyProperties(teacher, teacherDto);


            //department to departmentDto conversion
            DepartmentDto departmentDto = new DepartmentDto();

            if (teacher.getDepartment() == null) {
                departmentDto.setDepartmentName("Not Assigned");
            } else {
                BeanUtils.copyProperties(teacher.getDepartment(), departmentDto);
            }
            teacherDto.setDepartmentDto(departmentDto);

            teacherDtoList.add(teacherDto);
        }
        return teacherDtoList;
    }


}
