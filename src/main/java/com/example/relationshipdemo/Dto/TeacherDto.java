package com.example.relationshipdemo.Dto;

import lombok.Data;


@Data
public class TeacherDto {
    private long teacherId;
    private String teacherName;
    private int age;
    private DepartmentDto departmentDto;


}
