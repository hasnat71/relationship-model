package com.example.relationshipdemo.Controller;

import com.example.relationshipdemo.Dto.DepartmentDto;
import com.example.relationshipdemo.Dto.TeacherDto;
import com.example.relationshipdemo.Entity.Course;
import com.example.relationshipdemo.Entity.Department;
import com.example.relationshipdemo.Entity.Teacher;
import com.example.relationshipdemo.Service.CourseService;
import com.example.relationshipdemo.Service.DepartmentService;
import com.example.relationshipdemo.Service.TeacherService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/dept")
public class DepartmentController {
    @Autowired
    DepartmentService departmentService;
    @Autowired
    TeacherService teacherService;
    @Autowired
    CourseService courseService;

    @GetMapping("/add")
    public String addDept(Model model) {
        DepartmentDto departmentDto = new DepartmentDto();
        model.addAttribute("deptDtoForm", departmentDto);

        List<Teacher> teacherList = teacherService.getAllTeacher();

        model.addAttribute("teacherListObj", generateTeacherDtoList(teacherList));

        List<Course> courseList = courseService.getAllCourse();
        model.addAttribute("courseListObj", courseList);
        return "department/add";
    }

    // save department and also assign some students
    @PostMapping("/save")
    public String saveDept(@ModelAttribute DepartmentDto deptDtoForm) {
        Department department = new Department();
        BeanUtils.copyProperties(deptDtoForm, department);

        List<Teacher> teacherList = new ArrayList<>();
        for (long teacherId : deptDtoForm.getTeacherIdList()) {
            Teacher teacher = new Teacher();
            teacher = teacherService.findTeacherById(teacherId);
            teacherList.add(teacher);
        }
        department.setTeacherList(teacherList);


        List<Course> courseList = new ArrayList<>();
        for (long courseId : deptDtoForm.getCourseIdList()) {
            Course course = new Course();
            course = courseService.findCourseById(courseId);
            courseList.add(course);
        }
        department.setCourseList(courseList);

        departmentService.save(department);
        return "redirect:/dept/add";
    }


    // Helper Method
    private List<TeacherDto> generateTeacherDtoList(List<Teacher> teacherList) {
        List<TeacherDto> teacherDtoList = new ArrayList<>();
        for (Teacher teacher : teacherList) {
            if (teacher.getDepartment() == null) {
                TeacherDto teacherDto = new TeacherDto();
                BeanUtils.copyProperties(teacher, teacherDto);
                teacherDtoList.add(teacherDto);
            }
        }
        return teacherDtoList;
    }

}
