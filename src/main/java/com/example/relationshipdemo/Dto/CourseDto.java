package com.example.relationshipdemo.Dto;

import lombok.Data;

@Data
public class CourseDto {
    private long courseId;
    private String courseName;
    private String courseCredit;

}
