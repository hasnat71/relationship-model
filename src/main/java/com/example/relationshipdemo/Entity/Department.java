package com.example.relationshipdemo.Entity;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
@Entity(name = "department")
public class Department {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long departmentId;
    @Column
    private String departmentName;

    @OneToMany
    @JoinTable(name = "department_teacher",
            joinColumns = @JoinColumn(name = "departmentId"),
            inverseJoinColumns = @JoinColumn(name = "teacherId"))
    private List<Teacher> teacherList;

    @ManyToMany
    @JoinTable(name = "department_course",
    joinColumns = @JoinColumn(name = "departmentId"),
    inverseJoinColumns = @JoinColumn(name = "courseId"))
    private List<Course> courseList;





}
