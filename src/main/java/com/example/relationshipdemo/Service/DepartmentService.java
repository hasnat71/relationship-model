package com.example.relationshipdemo.Service;

import com.example.relationshipdemo.Entity.Department;
import com.example.relationshipdemo.Repository.DepartmentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DepartmentService {
    @Autowired
    DepartmentRepository departmentRepository;

    public void save(Department department) {
        departmentRepository.save(department);
    }
}
