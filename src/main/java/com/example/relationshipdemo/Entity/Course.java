package com.example.relationshipdemo.Entity;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity(name = "course")
public class Course {
   @Id
   @GeneratedValue(strategy = GenerationType.IDENTITY)
   private long courseId;
   @Column
   private String courseName;
    @Column
    private String courseCredit;

}
