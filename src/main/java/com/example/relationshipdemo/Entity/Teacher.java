package com.example.relationshipdemo.Entity;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
@Entity(name = "teacher")
public class Teacher {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long teacherId;
    @Column
    private String teacherName;
    @Column
    private int age;

    @ManyToOne
    @JoinTable(name = "department_teacher",
            joinColumns = @JoinColumn(name = "teacherId"),
            inverseJoinColumns = @JoinColumn(name = "departmentId"))
    private Department department;
}
