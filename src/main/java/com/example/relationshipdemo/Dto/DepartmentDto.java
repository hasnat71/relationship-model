package com.example.relationshipdemo.Dto;

import com.example.relationshipdemo.Entity.Course;
import com.example.relationshipdemo.Entity.Teacher;
import lombok.Data;

import javax.persistence.Column;
import java.util.List;

@Data
public class DepartmentDto {
    private long departmentId;
    private String departmentName;
    private List<Teacher> teacherList;
    private List<Course> courseList;

    private List<Long> teacherIdList;
    private List<Long> courseIdList;
}
