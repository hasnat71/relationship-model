package com.example.relationshipdemo.Controller;

import com.example.relationshipdemo.Dto.CourseDto;
import com.example.relationshipdemo.Entity.Course;
import com.example.relationshipdemo.Service.CourseService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/course")
public class CourseController {
    @Autowired
    CourseService courseService;

    @GetMapping("/addCourse")
    public String addCourse(Model model) {
        CourseDto courseDto = new CourseDto();
        model.addAttribute("courseDtoForm", courseDto);
        return "course/add";
    }

    @PostMapping("/save")
    public String saveStudent(@ModelAttribute CourseDto courseDtoForm) {

        Course course = new Course();
        BeanUtils.copyProperties(courseDtoForm, course);
        courseService.save(course);
        return "redirect:/course/addCourse";
    }
}
