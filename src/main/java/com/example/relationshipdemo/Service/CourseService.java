package com.example.relationshipdemo.Service;

import com.example.relationshipdemo.Entity.Course;
import com.example.relationshipdemo.Repository.CourseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CourseService {
    @Autowired
    CourseRepository courseRepository;

    public void save(Course course) {
        courseRepository.save(course);
    }

    public List<Course> getAllCourse(){
       return courseRepository.findAll();
    }

    public Course findCourseById(long courseId) {
       return courseRepository.getOne(courseId);
    }
}
